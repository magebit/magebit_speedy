<?php
/**
 * Magebit_Speedy
 *
 * @category     Magebit
 * @package      Magebit_Speedy
 * @author       Mairis Kimenis <info@magebit.com>
 * @copyright    Copyright (c) 2016 Magebit, Ltd. (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Magebit_Speedy_Model_Catalog_Product_Type_Configurable extends Mage_Catalog_Model_Product_Type_Configurable
{
    /**
     * Major speed improvements on huge sites
     *
     * @param null $product
     * @return bool
     */
    public function isSalable($product = null)
    {
        if (!Mage::getStoreConfig('speedy/general/enable_issalable')) {
            return parent::isSalable($product);
        }

        $salable = Mage_Catalog_Model_Product_Type_Abstract::isSalable($product);

        if ($salable !== false) {
            if (!Mage::app()->getStore()->isAdmin() && $product) {
                $collection = Mage::getResourceModel('catalog/product_type_configurable_product_collection')
                    ->setFlag('product_children', true)
                    ->setProductFilter($this->getProduct($product))
                    ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                    ->addStoreFilter($product->getStoreId());

                // only fetch products in stock
                Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);

                return $collection->getSize() > 0;
            }

            return parent::isSalable($product);
        }

        return $salable;
    }
}
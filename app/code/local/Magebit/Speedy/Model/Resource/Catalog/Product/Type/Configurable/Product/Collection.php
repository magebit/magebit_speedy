<?php
/**
 * Magebit_Speedy
 *
 * @category     Magebit
 * @package      Magebit_Speedy
 * @author       Mairis Kimenis <info@magebit.com>
 * @copyright    Copyright (c) 2016 Magebit, Ltd. (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Magebit_Speedy_Model_Resource_Catalog_Product_Type_Configurable_Product_Collection extends Mage_Catalog_Model_Resource_Product_Type_Configurable_Product_Collection
{
    /**
     * Corrected for configurable products
     *
     * @return bool
     */
    public function isEnabledFlat()
    {
        if (!Mage::getStoreConfig('speedy/general/enable_configurable_flat')) {
            return parent::isEnabledFlat();
        }

        return Mage_Catalog_Model_Resource_Product_Collection::isEnabledFlat();
    }

    /**
     * Fixes addAttributeToFilter
     *
     * @param Mage_Eav_Model_Entity_Attribute_Abstract|string $attribute
     * @param null                                            $condition
     * @param string                                          $joinType
     * @return Mage_Catalog_Model_Resource_Product_Collection
     * @throws Mage_Core_Exception
     */
    public function addAttributeToFilter($attribute, $condition = null, $joinType = 'inner')
    {
        if (!Mage::getStoreConfig('speedy/general/enable_configurable_flat')) {
            return parent::addAttributeToFilter($attribute, $condition, $joinType);
        }

        if ($this->isEnabledFlat() && is_numeric($attribute)) {
            $attribute = $this->getEntity()->getAttribute($attribute)->getAttributeCode();
        }
        return parent::addAttributeToFilter($attribute, $condition, $joinType);
    }
}
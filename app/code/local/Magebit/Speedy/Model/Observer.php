<?php
/**
 * Magebit_Speedy
 *
 * @category     Magebit
 * @package      Magebit_Speedy
 * @author       Mairis Kimenis <info@magebit.com>
 * @copyright    Copyright (c) 2016 Magebit, Ltd. (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Magebit_Speedy_Model_Observer
{

    /**
     * Disable logging and reporting
     *
     * @return void
     */
    public function controllerFrontInitBefore()
    {
        if (Mage::getStoreConfig('speedy/general/disable_logging')) {
            $this->disableLoggingEvents();
        }

        if (Mage::getStoreConfig('speedy/general/disable_reports')) {
            $this->disableReportEvents();
        }
    }

    /**
     * Speed up page by removing dynamic layout handles
     *
     * @param Varien_Event_Observer $observer
     * @return void
     */
    public function controllerActionLayoutLoadBefore(Varien_Event_Observer $observer)
    {
        if (!Mage::getStoreConfig('speedy/general/disable_layouts')) {
            return;
        }

        $update = $observer->getLayout()->getUpdate();

        foreach ($update->getHandles() as $handle) {
            if (strpos($handle, 'CATEGORY_') === 0 || (strpos($handle, 'PRODUCT_') === 0 && strpos($handle,
                        'PRODUCT_TYPE') === false)
            ) {
                $update->removeHandle($handle);
            }
        }
    }

    /**
     * Enables caching for various blocks
     *
     * @param Varien_Event_Observer $observer
     * @return void
     */
    public function coreBlockAbstractToHtmlBefore(Varien_Event_Observer $observer)
    {
        /** @var Mage_Core_Block_Abstract $block */
        $block = $observer->getBlock();

        if ($block instanceof Mage_Cms_Block_Block || $block instanceof Mage_Cms_Block_Widget_Block) {
            $this->enableCmsBlockCache($block);
        } elseif ($block instanceof Mage_Page_Block_Html_Topmenu) {
            $this->enableTopmenuCache($block);
        }
    }

    /**
     * Enables cms block caching
     *
     * @param $block
     * @return void
     */
    protected function enableCmsBlockCache($block)
    {
        if (!Mage::getStoreConfig('speedy/general/enable_cms_cache')) {
            return;
        }

        $key = array(
            'BLOCK_TPL',
            Mage::app()->getStore()->getCode(),
            $block->getBlockId()
        );
        $key = implode('|', $key);
        $key = sha1($key);

        $block->setCacheKey($key);
        $block->setCacheLifetime(false);
        $block->setCacheTags(array(Mage_Cms_Model_Block::CACHE_TAG));
    }

    /**
     * Enables cms block caching
     *
     * @param $block
     * @return void
     */
    protected function enableTopmenuCache($block)
    {
        if (!Mage::getStoreConfig('speedy/general/enable_topmenu_cache')) {
            return;
        }

        $key = array(
            'BLOCK_TPL',
            Mage::app()->getStore()->getCode(),
            'NAVIGATION',
            Mage::registry('current_category') ?
                Mage::registry('current_category')->getId() : Mage::helper('core/url')->getCurrentUrl()
        );
        $key = implode('|', $key);
        $key = sha1($key);

        $block->setCacheKey($key);
        $block->setCacheLifetime(false);
        $block->setCacheTags(array(Mage_Catalog_Model_Category::CACHE_TAG));
    }

    /**
     * Disables all logging events
     *
     * @return void
     */
    protected function disableLoggingEvents()
    {
        $events = array(
            'controller_action_predispatch',
            'controller_action_postdispatch',
            'customer_login',
            'customer_logout',
            'sales_quote_save_after',
            'checkout_quote_destroy'
        );

        foreach ($events as $event) {
            $path = 'frontend/events/' . $event . '/observers/log/type';
            Mage::app()->getConfig()->setNode($path, 'disabled');
        }
    }

    /**
     * Disables all report events
     *
     * @return void
     */
    protected function disableReportEvents()
    {
        $events = array(
            'catalog_product_compare_remove_product',
            'catalog_controller_product_view',
            'customer_login',
            'customer_logout',
            'sendfriend_product',
            'catalog_product_compare_add_product',
            'catalog_product_compare_item_collection_clear',
            'sales_quote_item_save_before',
            'wishlist_add_product',
            'wishlist_share'
        );

        foreach ($events as $event) {
            $path = 'frontend/events/' . $event . '/observers/reports/type';
            Mage::app()->getConfig()->setNode($path, 'disabled');
        }
    }
}